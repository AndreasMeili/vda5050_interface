import time

from meili_sdk.mqtt.client import MeiliMqttClient
from meili_sdk.mqtt.models.state import (
    
    VDA5050NodeState,
    VDA5050StateMessage,
    VDA5050VehicleBatteryState,
)
import threading
import time

class Meili_VDA5050():
    """
    Class for interact with mqtt vda5050
    """

    def __init__(self):
        ########### VDA5050 #############
        # holds the future goal nodes
        self.nodes = []
        # is sent in status msg to master
        # nodePosition uses capital X, Y, Theta through program, 
        # but is changed to lower case when vda_send_status
        self.dummy_node = {"x": -0.5, "y": 0.0, "theta": 200}
        self.nodeStates = [
                    {
                        "nodeId": "1",
                        "sequenceId": "1",
                        "sequence": "1",
                        "nodePosition": self.dummy_node,
                        "released": True,
                    }
                ]
        # holds the last traversed nodeId
        self.last_node = self.nodeStates[0]

        self.pose = self.dummy_node

        # task logic
        self.is_driving = False # False is idle, True is driving to goal
        self.goal_status = 0 # 0 is 



        self.mqtt_client=None
        self.setup_token = None

        self.host="https://mqtt.meilirobots.com" #nosec

        # Capra Workshop
        self.setup_uuid = "20729943ca2e4141b7722f0d9ed96fad" #nosec
        self.vehicle_uuid = "9c28a315da3448749367f7a7ae09d18a" #nosec
        self.token = "714268e61698a6f39cb96cc1d952bf35aadde1b9" #nosec
        self.mqtt_id = "meili-agent-ec9f1360-cbce-4d6a-9c17-3ea0ec013558" #nosec

    def run(self):
        while(1):
            self.vda_send_status()
            time.sleep(1)


    def vda_send_status(self):
        try:
            message = VDA5050StateMessage(
                    manufacturer="Meili",
                    serialNumber=self.vehicle_uuid,
                    nodeStates=self.nodeStates,
                    agvPosition=self.pose,
                    lastNodeId=self.last_node["nodeId"],
                    lastNodeSequenceId=self.last_node["sequenceId"],
                    batteryState=VDA5050VehicleBatteryState(
                        batteryCharge=70, batteryVoltage=12, batteryHealth=1, charging=True
                    ),
                )

            self.mqtt_client.publish_to_state_topic(self.vehicle_uuid, message, qos=1)

            print("\nnode list:")
            print(f" - last node id: {self.last_node['nodeId']} | sequenceId: {self.last_node['sequenceId']}")
            
            for node in self.nodeStates:
                print(f" - node: {node['nodeId']}  |  position: {node['nodePosition']}")
            
            print("status sent:")
        except Exception as e:
            print(f" - vda_send_status error : {e}")
        


    def on_message(self, client, topic, raw_data, data):
        print("MESSAGE RECEIVED")
        # print(f"message in topic: {topic} \nreceived with the following data: {data}")
        if topic.endswith("orders"):
            print(f"order received")
            self.order_callback(client, topic, raw_data, data)

        elif topic.endswith("actions"):
            print(f"action received")
            self.action_callback(client, topic, raw_data, data)

    def order_callback(self, client, topic, raw_data, data):
        # received order from Meili, send to robot
        try:
            self.nodeStates = data["nodes"]
            for node in self.nodeStates:
                nodeid = node["nodeId"]
                node_x = float(node["nodePosition"]["X"])
                node_y = float(node["nodePosition"]["Y"])
                print(f"- node id: {nodeid}  node position: x:{node_x}, y:{node_y}")
        except Exception as e:
            print(f"error in order parsing, {e}")
        self.fix_difference_in_node_states()

    def action_callback(self, client, topic, raw_data, data):
        # received action from Meili, send to robot
        pass

    def state_callback(self, client, topic, raw_data, data):
        # received state from robot, send to Meili
        pass
        


    ########### MSG conversion ############
    def fix_difference_in_node_states(self):
        self.change_pos_to_lowercase()
        self.add_sequence()

    def add_sequence(self):
        fixed_nodes = []
        try:
            for i, node in enumerate(self.nodeStates):
                fixed_node = node
                sequenceId = node["sequenceId"]
                #print(f"\n sequence error: {sequenceId}")
                fixed_node.setdefault("sequence", node["sequenceId"])
                #print(f"checking the add_sequenceId {fixed_node}")
                fixed_nodes.append(fixed_node)
            self.nodeStates = fixed_nodes
        except Exception as e:
            print(f"add sequence error: {e}")

            
    def change_pos_to_lowercase(self):
        fixed_nodes = []
        for node in self.nodeStates:
            fixed_nodePosition = {
                "x" : node["nodePosition"]["X"],
                "y" : node["nodePosition"]["Y"],
                "theta" : node["nodePosition"]["Theta"]
            }
            x = fixed_nodePosition["x"]
            #print(f"type of x {type(x)}")
            fixed_node = node
            fixed_node["nodePosition"] = fixed_nodePosition
            fixed_nodes.append(fixed_node)
        #print(f" after changing to lower case: \n {fixed_nodes}")
        self.nodeStates = fixed_nodes


    # MQTT
    def on_open(self, *_):
        print(f"[MQTT] Connection was opened")

    def on_disconnect(self,*_):
        print("[MQTT] On disconect")
        print(_)


def main():
    vda5050=Meili_VDA5050()

    vda5050.mqtt_client = MeiliMqttClient(
             client_id=vda5050.mqtt_id,
             host=vda5050.host,
             token=vda5050.token,
             setup_uuid=vda5050.setup_uuid,
             port=1883,
             open_handler=vda5050.on_open,
             message_handler=vda5050.on_message,
             disconnect_handler=vda5050.on_disconnect,
        )

    print("run client")
   # thread1 = threading.Thread(target=vda5050.mqtt_client.run(), args=(False,), daemon=True)
   # thread1.start()

    vda5050.mqtt_client.run(block=False)
    vda5050.mqtt_client.wait_for_connection(10)
    try:
        print("[vda5050] Subscribing topics")
        vda5050.mqtt_client.subscribe_default_topics(f"{vda5050.vehicle_uuid}")

        #vda5050.mqtt_client.subscribe(f"meili/vehicle/{vda5050.vehicle_uuid}/orders")
        #vda5050.mqtt_client.subscribe(f"meili/vehicle/{vda5050.vehicle_uuid}/actions")
        print("[vda5050] Topics are subscribed")
    except Exception as error:
        print(error)

    
    print("[vda5050] Sending status in a seperate thread")
    thread = threading.Thread(
                target=vda5050.run(), daemon=True
            )
    thread.start()


if __name__ == "__main__":
    try:
        main()
    except Exception as error:
        print(error)

